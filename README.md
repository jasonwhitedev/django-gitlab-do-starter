# Django gitlab digitalocean starter

This is a starter project to help set up GitLab CI/CD for a new Django app.

17th Feb 2020 - version 0.1



### Features

- SSL for production

- Postgres db for development and production

- CI/CD - push updates to GitLab (master branch?) to run deployment to DO server.



# Run locally: 

You need to have docker and docker-compose installed...

- Set up your .env variables

- Run from the 'app' directory: docker-compose up

- Go to http://127.0.0.1:8000 - http://127.0.0.1:8000/admin321 - log in with your DJANGO_SU details



# Deploy to DO docker via GitLab

- Set up a GitLab account: https://gitlab.com/

- Add your SSH key to your account: https://gitlab.com/profile/keys

- Clone this project and push it to your own GitLab repo

- Set up the CI/CD environment variables - see here: .env-sample

- Set up a Docker droplet - minimum spec is fine: https://marketplace.digitalocean.com/apps/docker

- Create an ssh key - ~/.ssh/id_rsa

- Set DNS to point to your Docker droplet IP address - you will need a domain name / fqdn to get an SSL certificate



### Alter the following files:

- app/hello_django/settings.py - Add to ALLOWED_HOSTS - IP addresses and or FQDNs

- nginx/conf/nginx.conf - set server_name, ssl_certificate, ssl_certificate_key for your url/fqdn

- init-letsencrypt.sh - set url (domain) and email - Setting staging to '1' means that the SSL cerificate will be fake - set to '0' for a proper certificate



# Deploy

- Push code from your PC to your repo to kick-off the CI/CD 



# Set up SSL

- log into your DO server and run the init-letsencrypt.sh script - ./init-letsencrypt.sh - go through the prompts to have certbot generate an SSL certifcate.



# Todo

- Get init-letsencrypt.sh to run on first deployment - prevent this script needing user input to run

- Add a staging server/branch

- Only deploy to production if the new release is tagged/versioned?


# From:

Follow the tutorial from testdriven to help set this up:
- https://testdriven.io/blog/deploying-django-to-digitalocean-with-docker-and-gitlab/

- https://github.com/wmnnd/nginx-certbot

- Added postgres as a docker service, rather than as a managed digitalocean database

- Stopped the database being flushed on each deployment